#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;
void itempopuplate(vector<string>& items) {
	int itemNum;
	for (int i = 0; i < 10; i++) {
		itemNum = rand() % 4 + 1;
		if (itemNum == 1) {
			items.push_back("RedPotion");
		}
		else if (itemNum ==2) {
			items.push_back("Elixir");
		}
		else if (itemNum ==3) {
			items.push_back("EmptyBottle");
		}
		else if (itemNum ==4) {
			items.push_back("BluePotion");
		}
	}

}

void displayitems(const vector<string>& items) {
	for (int i = 0; i < items.size(); i++) {
		cout << i<<". " << items[i] << endl;
	}

}

int countTotalItems(const vector<string>& items) {
	int countRed = 0, countElix = 0, countEmpty = 0, countBlue = 0;
	string item;
	for (int i = 0; i < items.size(); i++) {
		item = items[i];
		if (item == "RedPotion") {
			countRed++;
		}
		else if (item == "Elixir") {
			countElix++;
		}
		else if (item == "EmptyBottle") {
			countEmpty++;
		}
		else if (item == "BluePotion") {
			countBlue++;
		}
	}
	cout << "You have a total of: " << countRed << " RedPotions" << endl;
	cout << "You have a total of: " << countElix << " Elixirs" << endl;
	cout << "You have a total of: " << countEmpty << " EmptyBottles" << endl;
	cout << "You have a total of: " << countBlue << " BluePotions" << endl;

	return countRed, countElix, countEmpty, countBlue;
}

void removeItem(vector<string>& items) {
	int inventorySlot;
	cout << "What item will you use?" << endl;
	cin >> inventorySlot;
	items.erase(items.begin() + inventorySlot);
}

void main() {
	srand(time(0));
	vector<string> items;
	itempopuplate(items);
	cout << "Here are your Starting Items: " << endl ;
	displayitems(items);
	cout << endl;
	countTotalItems(items);
	removeItem(items);
	system("Pause");
	system("cls");
	cout << "Total remaining items: " << endl;
	countTotalItems(items);
}
