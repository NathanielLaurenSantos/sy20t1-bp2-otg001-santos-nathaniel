#include <iostream>
#include <string>
#include <time.h>

using namespace std;




int Bet(int bet ,int& gold) {

	int total =gold -= bet;

	return total;
}


int diceroll(int& dice) {
	int dice1 = rand() % 6 + 1;
	int dice2 = rand() % 6 + 1;
		
	cout << "First Die " << dice1 << endl;
	cout << "Second Die " << dice2 << endl;
	int total = (dice1 += dice2);
	dice = total;
	return total;
}

int lose(int ai,int pc, int bet, int& gold) {
	if (ai > pc) {
	
		cout << "You Lose!" <<endl;
		return gold;
	}
	else {
		int total = gold += bet;
		cout << "You Win!" << endl;
		return total;
	}
}

void playRound(int& Gold) {
	int bet;
	int dieai = 0;
	int diepc = 0;
	cout << "Place your bet here: ";
	cin >> bet;
	cout << endl;
	Bet(bet, Gold);
	cout << "Your total gold is now " << Gold << endl;
	cout << endl;
	cout << "The AI rolls: " << endl;
	diceroll(dieai);
	cout << "Total roll is: " << dieai << endl;
	system("pause");
	cout << endl;
	cout << "You Rolled: " << endl;
	diceroll(diepc);
	cout << "Total roll is " << diepc << endl;
	system("pause");
	cout << endl;
	cout << endl;
	lose(dieai, diepc, bet, Gold);
	cout << endl;
	cout << endl;
	cout << "Your total gold at the end of this round is: " << Gold << endl;
	system("pause");
	system("cls");
}


void main() {
	int Gold = 1000;
	srand(time(0));

	string playerName;
	cout << "Insert Player Name: ";
	cin >> playerName;
		system("cls");
	cout << "Hello " << playerName << endl;
	while (Gold > 0) {
		playRound(Gold);
	}

	cout << "You have lost all your gold!" << endl;
	cout << "Thank you for playing!" << endl;
}