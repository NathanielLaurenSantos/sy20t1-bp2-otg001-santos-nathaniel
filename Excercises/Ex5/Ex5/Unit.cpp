#include "Unit.h"
#include "Skill.h"
#include <time.h>
using namespace std;
Unit::Unit(string name, int hp, int pwr, int vit, int dex, int agi)
{
	mName = name;
	mPWR = pwr;
	mVIT = vit;
	mDEX = dex;
	mAGI = agi;
	mHP = hp;

}

string Unit::getName()
{
	return mName;
}

int Unit::getHP()
{
	return mHP;
}

int Unit::getPWR()
{
	return mPWR;
}

int Unit::getVIT()
{
	return mVIT;
}

int Unit::getDEX()
{
	return mDEX;
}

int Unit::getAGI()
{
	return mAGI;
}

void Unit::buffHP(int buff)
{
	mHP = mHP + buff;
}

void Unit::buffPWR(int buff)
{
	mPWR = mPWR + buff;
}

void Unit::buffVIT(int buff)
{
	mVIT = mVIT + buff;
}

void Unit::buffDEX(int buff)
{
	mDEX = mDEX + buff;
}

void Unit::buffAGI(int buff)
{
	mAGI = mAGI + buff;
}

void Unit::useSkill(Unit* target,Skill*	skill)
{
	skill->effect(target);
}
