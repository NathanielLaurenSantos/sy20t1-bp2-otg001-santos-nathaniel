#pragma once
#include <iostream>
#include <string>
#include <vector>
class Skill;
using namespace std;

class Unit
{
public:
	Unit(string name, int hp, int pwr, int vit, int dex, int agi);

	string getName();
	int getHP();
	int getPWR();
	int getVIT();
	int getDEX();
	int getAGI();

	void buffHP(int buff);
	void buffPWR(int buff);
	void buffVIT(int buff);
	void buffDEX(int buff);
	void buffAGI(int buff);

	void useSkill(Unit* target, Skill* skill);

private:
	string mName;
	int mHP;
	int mPWR;
	int mVIT;
	int mDEX;
	int mAGI;
};

