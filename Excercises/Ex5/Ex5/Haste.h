#pragma once
#include "Skill.h"
#include "Unit.h"
using namespace std;
class Haste :
	public Skill
{
public:
	Haste();

	void effect(Unit* target) override;
};

