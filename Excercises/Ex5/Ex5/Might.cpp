#include "Might.h"
#include <iostream>
using namespace std;
Might::Might()
	:Skill("Might", 2)
{
}

void Might::effect(Unit* target)
{
	cout << "You have casted " << this->getName() << " on " << target->getName() << " and it gave + " << this->getSkillStrgnth() << " to Power" << endl;
	target->buffPWR(this->getSkillStrgnth());
}
