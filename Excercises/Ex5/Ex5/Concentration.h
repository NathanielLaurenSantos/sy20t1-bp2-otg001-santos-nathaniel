#pragma once
#include "Skill.h"
#include "Unit.h"
using namespace std;
class Concentration :
	public Skill
{
public:
	Concentration();

	void effect(Unit* target) override;
};

