#pragma once
#include "Skill.h"
#include "Unit.h"
using namespace std;
class IronSkin :
	public Skill
{
public:
	IronSkin();

	void effect(Unit* target) override;
};

