#include "Concentration.h"
#include <iostream>
using namespace std;
Concentration::Concentration()
	:Skill("Concentration", 2)
{
}

void Concentration::effect(Unit* target)
{
	cout << "You have casted " << this->getName() << " on " << target->getName() << " and it gave + " << this->getSkillStrgnth() << " to Dexterity" << endl;
	target->buffDEX(this->getSkillStrgnth());
}
