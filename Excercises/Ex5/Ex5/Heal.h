#pragma once
#include "Skill.h"
#include "Unit.h"
using namespace std;
class Heal :
	public Skill
{
	
public:
	Heal();
	void effect(Unit* target) override;
};

