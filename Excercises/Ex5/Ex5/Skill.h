#pragma once
#include <string>
#include <iostream>
#include "Unit.h"

using namespace std;

class Skill
{
public:
	Skill(string name, int strgth);

	string getName();
	int getSkillStrgnth();
	virtual void effect(Unit* target);

private:
	string mName;
	int mSkillStrgth;
};

