#pragma once
#include "Skill.h"
#include "Unit.h"
using namespace std;
class Might :
	public Skill
{
public:
	Might();

	void effect(Unit* target) override;
};

