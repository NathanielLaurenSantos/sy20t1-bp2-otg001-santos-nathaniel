#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include "Unit.h"
#include "Skill.h"
#include "Might.h"
#include "IronSkin.h"
#include "Heal.h"
#include "Haste.h"
#include "Concentration.h"

using namespace std;
void printStat(Unit* player) {
	cout << "Name: " << player->getName() << endl;
	cout << "HP : " << player->getHP() << endl;
	cout << "PWR: " << player->getPWR() << endl;
	cout << "VIT: " << player->getVIT() << endl;
	cout << "DEX: " << player->getDEX() << endl;
	cout << "AGI: " << player->getAGI() << endl;
}
void main() {
	srand(time(NULL));
	string name;
	vector<Skill*> skillSet;
	Heal* heal = new Heal();
	Might* might = new Might();
	IronSkin* ironSkin = new IronSkin();
	Concentration* concentration = new Concentration();
	Haste* haste = new Haste();

	skillSet.push_back(heal);
	skillSet.push_back(might);
	skillSet.push_back(concentration);
	skillSet.push_back(haste);
	skillSet.push_back(ironSkin);

	cout << "Enter your name" << endl;
	cin >> name;
	Unit* player = new Unit(name, 50, 10, 10, 10, 10);

	while (true) {
		int skillRandomizer = rand() % 5;

		Skill* skill = skillSet[skillRandomizer];
		printStat(player);
		player->useSkill(player, skill);
		system("pause");
		system("cls");
	}
}