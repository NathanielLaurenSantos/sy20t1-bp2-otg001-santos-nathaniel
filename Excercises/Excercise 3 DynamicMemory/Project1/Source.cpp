#include<iostream>
#include<string>
#include<time.h>
#include<vector>

using namespace std;

struct item {

	string name;
	int gold;
};

item* generateRandomItem() {
	int itemNum = rand() % 5 + 1;
	item* generatedItem = new item;	
	if (itemNum == 1) {
	generatedItem->name = "Mithril Ore";
	generatedItem->gold = 100;
	}
	else if (itemNum == 2) {
	generatedItem->name = "Sharp Talon";
	generatedItem->gold = 50;
	}
	else if (itemNum== 3) {
	generatedItem->name = "Thick Leahter";
	generatedItem->gold = 25;
	}
	else if (itemNum == 4) {
	generatedItem->name = "Jellopy";
	generatedItem->gold = 5;
	}
	else if (itemNum == 5) {
	generatedItem->name = "Cursed Stone";
	generatedItem->gold = 0;
	}
	return generatedItem;
}

int goldCalculations(int multiplier, int& currentlyOwn, vector<item*> inventory, int inventoryNum) {
	int goldToAdd = inventory[inventoryNum]->gold * multiplier;
	
	currentlyOwn = currentlyOwn + goldToAdd;
	return currentlyOwn;
}

int enterDungeon(vector<item*>& inventory, int& gold) {
	int goldMultiplier = 1;
	bool notCursed = true;
	string deeper;
	gold = gold - 25;
	do {
		item* recievedItem = generateRandomItem();
		system("cls");
		cout << "Current Gold: " << gold << endl;
		if (recievedItem->name != "Cursed Stone") {
			cout << "Oh you got a " << recievedItem->name << "!" << endl;
			cout << "It is worth " << recievedItem->gold << " Gold" << endl;
			inventory.push_back(recievedItem);
			cout << "Do you want to go deeper into the dungeon(Yes/No)" << endl;
			cin >> deeper;
			if (deeper == "Yes") {
				if (goldMultiplier < 4) {
				goldMultiplier++;
				}
			}
			else if (deeper == "No") {
				for (int i = 0; i < inventory.size(); i++) {
					goldCalculations(goldMultiplier, gold, inventory, i);
				}
				return gold;
			}
		}
		else {
			cout << "Oh no! The Cursed Stone has sucked in your soul!" << endl;
			cout << "Your are now bieng sent back to the entrance of the dungeon!" << endl;
			system("pause");
			system("cls");
			notCursed = !notCursed;
		}
	}
	while (notCursed == true);
}

void main() {
	srand(time(NULL));
	int gold = 50;
	string question;	
	vector<item*> inventory;
	while (gold >= 25 && gold < 500) {
		system("cls");
		cout << "Current Gold: " << gold << endl;
		cout << "Would you like to enter the Dungeon??? (Yes/No)" << endl;
		cin >> question;
		if (question == "Yes") {
			enterDungeon(inventory, gold);
		}
	}
	if (gold >= 500) {
		cout << "Current Gold: " << gold << endl;
		cout << " You win!! You got the desired ammount for the Quest!!!" << endl;
	}
	else if (gold <25) {
		cout << "Current Gold: " << gold << endl;
		cout << " You dont have enough gold to enter the dungeon!" << endl;
	
	}
}