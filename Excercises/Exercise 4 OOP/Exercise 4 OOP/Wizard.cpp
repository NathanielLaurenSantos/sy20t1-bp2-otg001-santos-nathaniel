#include "Wizard.h"
#include <iostream>
#include <time.h>

void Wizard::attack(Wizard* target)
{

	srand(time(NULL));
	int manaAdd = rand() % 9 + 10;
	mMinMaxDamage = rand() % 4 + 10;

	mMp = mMp += manaAdd ;
	target->mHp -= mMinMaxDamage;
	cout << mName << " attacked " << target->mName << " for " << mMinMaxDamage << endl;
	cout << "This attack has generated " << manaAdd << " mana" << endl;
}
