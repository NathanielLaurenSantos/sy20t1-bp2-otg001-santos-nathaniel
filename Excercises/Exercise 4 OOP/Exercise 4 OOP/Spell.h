#pragma once
#include<string>
using namespace std;
class Wizard;

class Spell
{
public:
	string mName;
	int mMinMaxDamage;
	int mMpCost;

	void activate(Wizard* caster, Wizard* target);
};

