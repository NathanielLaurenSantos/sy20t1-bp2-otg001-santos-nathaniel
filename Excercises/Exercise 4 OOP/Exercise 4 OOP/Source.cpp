#include <iostream>
#include <string>
#include <time.h>
#include "Wizard.h"
#include "Spell.h"
using namespace std;

void main() {
	Wizard* pyroMancer = new Wizard();
	pyroMancer->mName = "Pyromancer";
	pyroMancer->mHp = 250;
	pyroMancer->mMp = 0;

	Wizard* cryoMancer = new Wizard();
	cryoMancer->mName = "Cryomancer";
	cryoMancer->mHp = 250;
	cryoMancer->mMp = 0;

	Spell* fireBall = new Spell();
	fireBall->mName = "Fire Ball";
	fireBall->mMpCost = 50;

	Spell* iceSpear = new Spell();
	iceSpear->mName = "Ice Spear";
	iceSpear->mMpCost = 50;

	while (pyroMancer->mHp > 0 && cryoMancer->mHp > 0) {
		system("cls");
		cout << pyroMancer->mName << "	||" << cryoMancer->mName << endl;
		cout << "HP:" << pyroMancer->mHp << "		||HP:" << cryoMancer->mHp << endl;
		cout << "MP:" << pyroMancer->mMp << "		||MP:" << cryoMancer->mMp << endl;
		
		if (pyroMancer->mMp >= fireBall->mMpCost) {
			fireBall->activate(pyroMancer, cryoMancer);
		}
		else {
			pyroMancer->attack(cryoMancer);
		}
		system("pause");
		system("cls");

		if (cryoMancer->mHp <= 0) {
			break;
		}

		cout << pyroMancer->mName << "	||" << cryoMancer->mName << endl;
		cout << "HP:" << pyroMancer->mHp << "		||HP:" << cryoMancer->mHp << endl;
		cout << "MP:" << pyroMancer->mMp << "		||MP:" << cryoMancer->mMp << endl;

		if (cryoMancer->mMp >= iceSpear->mMpCost) {
			iceSpear->activate(cryoMancer, pyroMancer);
		}
		else {
			cryoMancer->attack(pyroMancer);
		}
		system("pause");
	}

	if (pyroMancer ->mHp <= 0) {
		cout << pyroMancer->mName << " has been defeated by " << cryoMancer->mName << endl;
		delete pyroMancer;
		delete fireBall;
	}
	else if (cryoMancer ->mHp <= 0) {
		cout << cryoMancer->mName << " has been defeated by " << pyroMancer->mName << endl;
		delete cryoMancer;
		delete iceSpear;
	}
}