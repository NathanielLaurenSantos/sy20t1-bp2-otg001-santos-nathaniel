#include "Spell.h"
#include "Wizard.h"
#include <iostream>
#include <time.h>

void Spell::activate(Wizard* caster, Wizard* target)
{
	srand(time(NULL));
	mMinMaxDamage = rand() % 19 + 40;
	if (caster->mMp >= 50) {
		cout << caster->mName << " has casted " << mName << " targeting " << target->mName << endl;
		caster->mMp = caster->mMp -= mMpCost;
		cout << target->mName << " will take " << mMinMaxDamage << " damage" << endl;
		target->mHp = target->mHp -= mMinMaxDamage;
	}
}
