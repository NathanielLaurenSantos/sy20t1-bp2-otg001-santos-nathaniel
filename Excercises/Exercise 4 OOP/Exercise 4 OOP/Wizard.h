#pragma once
#include <string>

using namespace std;

class Wizard
{
public:
	string mName;
	int mHp;
	int mMp;
	int mMinMaxDamage;

	void attack(Wizard* target);
};

