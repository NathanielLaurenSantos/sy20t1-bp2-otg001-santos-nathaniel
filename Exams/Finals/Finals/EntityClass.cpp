#include "EntityClass.h"
#include <string>
#include <iostream>
#include <time.h>
#include "Item.h"
#include "Armor.h"

using namespace std;

EntityClass::EntityClass(string name, string pclass, int hp, int pow, int vit, int dex, int agi, Item* weapon, Armor* armor)
{
	mName = name;
	mClass = pclass;
	mMaxHP = hp;
	mCurrentHP = hp;
	mPow = pow;
	mVit = vit;
	mDex = dex;
	mAgi = agi;
	mWeapon = weapon;
	mArmor = armor;
}

string EntityClass::getName()
{
	return mName;
}

string EntityClass::getClass()
{
	return mClass;
}

int EntityClass::getCurrentHP()
{
	return mCurrentHP;
}

int EntityClass::getMaxHP()
{
	return mMaxHP;
}

int EntityClass::getPow()
{
	return mPow;
}

int EntityClass::getVit()
{
	return mVit;
}

int EntityClass::getDex()
{
	return mDex;
}

int EntityClass::getAgi()
{
	return mAgi;
}

int EntityClass::getExp()
{
	return mExp;
}

int EntityClass::getLevel()
{
	return mLevel;
}

int EntityClass::getReqExp()
{
	return mRequiredExp;
}

int EntityClass::getGold()
{
	return mGold;
}

Item* EntityClass::getWeapon()
{
	return mWeapon;
}

Armor* EntityClass::getArmor()
{
	return mArmor;
}

void EntityClass::Rest()
{
	mCurrentHP = mMaxHP;
}

void EntityClass::setHP(float value)
{
	mCurrentHP = value;
	if (mCurrentHP < 0) mCurrentHP = 0;
}

void EntityClass::buffMaxHP(int buff)
{
	mMaxHP = mMaxHP + buff;
}

void EntityClass::buffPow(int buff)
{
	mPow = mPow + buff;
}

void EntityClass::buffVit(int buff)
{
	mVit = mVit + buff;
}

void EntityClass::buffDex(int buff)
{
	mDex = mDex + buff;
}

void EntityClass::buffAgi(int buff)
{
	mAgi = mAgi + buff;
}

void EntityClass::addExp(int xpGain)
{
	mExp = mExp + xpGain;
}

void EntityClass::addGold(int goldGain)
{
	mGold = mGold + goldGain;
}

void EntityClass::goldLost(int lost)
{
	mGold = mGold - lost;
}

void EntityClass::setWeapon(Item* newWeapon)
{
	delete mWeapon;
	mWeapon = newWeapon;
}

void EntityClass::setArmor(Armor* newArmor)
{
	delete mArmor;
	mArmor = newArmor;
}

void EntityClass::attack(EntityClass* target)
{
	srand(time(NULL));
	int damage = 0;

	//Chance of hitting
	int hitChance = ((float)mDex / (float)target->getAgi()) * 100;
	if (hitChance > 80) {
		hitChance = 80;
	}
	else if (hitChance <20) {
		hitChance = 20;
	}

	int dodgeChance = rand() % 59 + 20;

	//Damage Calculations
	if (hitChance > dodgeChance) {
		damage = ((mPow + mWeapon->getDMG()) - (target->getVit() + target->getArmor()->getDef()));
		cout << mName << " landed a hit! Dealing " << damage << " to " << target->getName() << endl;
		target->setHP(target->getCurrentHP() - damage);
	}
	else {
		cout << mName << " attack missed!" << endl;
	}

}
