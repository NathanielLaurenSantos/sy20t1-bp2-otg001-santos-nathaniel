#include <string>
#include <iostream>
#include <time.h>
#include <vector>
#include "EntityClass.h"
#include "Armor.h"
#include "Item.h"

using namespace std;

EntityClass* generatePlayer(string name) {
	string playerClass;


	Item* weapon = new Item("Rusty Sword", 5, 0);
	Armor* armor = new Armor("Cloth Armor", 5, 0);
	while (playerClass != "Warrior" || "Thief" || "Crusader") {
		cout << "What is class you traveler? (Warrior||Thief||Crusader)" << endl;
		cin >> playerClass;
		if (playerClass == "Warrior") {
			EntityClass* warrior = new EntityClass(name, playerClass, 40, 10, 4, 4, 3, weapon, armor);
			return warrior;
		}
		else if (playerClass == "Thief") {
			EntityClass* thief = new EntityClass(name, playerClass, 30, 8, 3, 8, 8, weapon, armor);
			return thief;
		}
		else if (playerClass == "Crusader") {
			EntityClass* crusader = new EntityClass(name, playerClass, 50, 7, 7, 3, 3, weapon, armor);
			return crusader;
		}
		else {
			cout << "That is not a valid class. I'll ask you again." << endl;
		}
		system("pause");
		system("cls");
	}
}

EntityClass* generateEnemy(int type) {
	if(type >= 1 && type <= 5) {
		Item* cleaver = new Item("Bone Cleaver", 10, 0);
		Armor* hide = new Armor("Scale Hid", 10, 0);
		EntityClass* lord = new EntityClass("Orc Lord", "Lord", 50, 10, 6, 7, 7, cleaver, hide);
		return lord;
	}
	else if (type >= 6 && type <= 30) {
		Item* wood = new Item("Wooden Club", 4, 0);
		Armor* loin = new Armor("Loin Cloth", 2, 0);
		EntityClass* goblin = new EntityClass("Goblin", "Small Monster", 30, 5, 1, 2, 2, wood, loin);
		return goblin;
	}
	else if(type >=31 && type<=55){
		Item* iron = new Item("Iron Axe", 5, 0);
		Armor* leather = new Armor("Hard Leather", 5, 0);
		EntityClass* ogre = new EntityClass("Ogre", "Large Monster", 40, 6, 4, 3, 3, iron, leather);
		return ogre;
	}
	else if (type >= 56 && type <= 80) {
		Item* twin = new Item("Twin Axes", 7, 0);
		Armor* skin = new Armor("Hard Skin", 5, 0);
		EntityClass* orc = new EntityClass("Orc", "Medium Monster", 40, 8, 2, 4, 4, twin, skin);
		return orc;
	}
} 

void showStat(EntityClass* target) {
	cout << "Name: " << target->getName() << endl;
	cout << "Class: " << target->getClass() << endl;
	cout << "Level: " << target->getLevel() << endl;
	cout << "EXP: " << target->getExp() << endl;
	cout << "Required EXP to Level up " << target->getReqExp() << endl;
	cout << "HP: " << target->getCurrentHP() << "/" << target->getMaxHP() << endl;
	cout << "POW: " << target->getPow() << endl;
	cout << "VIT: " << target->getVit() << endl;
	cout << "DEX: " << target->getDex() << endl;
	cout << "AGI: " << target->getAgi() << endl;
	cout << "Weapon: " << target->getWeapon()->getName() << "||" << target->getWeapon()->getDMG() << endl;
	cout << "Armor: " << target->getArmor()->getName() << "||" << target->getArmor()->getDef() << endl;
}

void showOppStat(EntityClass* target) {
	cout << "Name: " << target->getName() << endl;
	cout << "Class: " << target->getClass() << endl;
	cout << "HP: " << target->getCurrentHP() << "/" << target->getMaxHP() << endl;
	cout << "POW: " << target->getPow() << endl;
	cout << "VIT: " << target->getVit() << endl;
	cout << "DEX: " << target->getDex() << endl;
	cout << "AGI: " << target->getAgi() << endl;
	cout << "Weapon: " << target->getWeapon()->getName() << "||" << target->getWeapon()->getDMG() << endl;
	cout << "Armor: " << target->getArmor()->getName() << "||" << target->getArmor()->getDef() << endl;
}

void battle(EntityClass* player, EntityClass* enemy) {
	while (player->getCurrentHP() != 0 && enemy->getCurrentHP() != 0) {
		int battleChoice;
		cout << "An Enemy has appeared!" << endl;
		showOppStat(enemy);
		cout << "What will you do?" << endl;
		cout << "1. Attack	2. Run" << endl;
		cin >> battleChoice;
		if (battleChoice == 1) {
			player->attack(enemy);
		}
		else if (battleChoice == 2) {
			int runChance = rand() % 99;
			if (runChance >= 0 && runChance <= 25) {
				cout << "You have successfully ran away" << endl;
				break;
			}
		}
		enemy->attack(player);
		system("pause");
		system("cls");
	}
}

void shop(EntityClass* player) {
	int shopChoice;
	int itemChoice;
	while (true) {
		cout << "Current Gold: " << player->getGold() << endl;
		cout << "Welcome to the shop! What will you buy?" << endl;
		cout << "1. Buy Weapon	2. Buy Armor	3. Leave Shop" << endl;
		cin >> shopChoice;
		if (shopChoice == 1) {
			cout << "Here's what's in the store today: " << endl;
			cout << "1) Short Sword: 5 damage		Cost: 10 Gold" << endl;
			cout << "2) Long Sword: 10 damage		Cost: 50 Gold" << endl;
			cout << "3) Broad Sword: 20 damage		Cost: 200 Gold" << endl;
			cout << "4)  Excalibur: 999 damage		Cost: 9999 Gold" << endl;
			cin >> itemChoice;
			if (itemChoice==1) {
				if (player->getGold() < 10) {
					cout << "I'm sorry but you dont have enough gold to purchase this" << endl;
				}
				else {
					Item* sword = new Item("Short Sword", 5, 10);
					player->setWeapon(sword);
					player->goldLost(sword->getCost());
					cout << "Thank you for purchasing this item!" << endl;
				}
			}
			else if (itemChoice == 2) {
				if (player->getGold() < 50) {
					cout << "I'm sorry but you dont have enough gold to purchase this" << endl;
				}
				else {
					Item* lSword = new Item("Long Sword", 10, 50);
					player->setWeapon(lSword);
					player->goldLost(lSword->getCost());
					cout << "Thank you for purchasing this item!" << endl;
				}
			}
			else  if (itemChoice == 3) {
				if (player->getGold() < 200) {
					cout << "I'm sorry but you dont have enough gold to purchase this" << endl;
				}
				else{
					Item* bSword = new Item("Broad Sword", 20, 200);
					player->setWeapon(bSword);
					player->goldLost(bSword->getCost());
					cout << "Thank you for purchasing this item!" << endl;
				}
			}
			else if (itemChoice == 4) {
				if (player->getGold() < 900) {
					cout << "I'm sorry but you dont have enough gold to purchase this" << endl;
				}
				else {
					Item* excal = new Item("Excalibur", 999, 9999);
					player->setWeapon(excal);
					player->goldLost(excal->getCost());
				}
			}
		}
		else if (shopChoice == 2) {
			cout << "Here's what's in the store today: " << endl;
			cout << "1) Leather Mail: 2 defence		Cost: 50 Gold" << endl;
			cout << "2) Chain Mail: 4 defence		Cost: 100 Gold" << endl;
			cout << "3) Plate Armor: 8 defence		Cost: 300 Gold" << endl;
			cin >> itemChoice;
			if (itemChoice == 1) {
				if (player->getGold() < 50) {
					cout << "I'm sorry but you dont have enough gold to purchase this" << endl;
				}
				else {
					Armor* lMail = new Armor("Leather Mail", 2, 50);
					player->setArmor(lMail);
					player->goldLost(lMail->getCost());
					cout << "Thank you for purchasing this item!" << endl;
				}
			}
			else if (itemChoice == 2) {
				if (player->getGold() < 100) {
					cout << "I'm sorry but you dont have enough gold to purchase this" << endl;
				}
				else {
					Armor* cMail = new Armor("Chain Mail", 4, 100);
					player->setArmor(cMail);
					player->goldLost(cMail->getCost());
					cout << "Thank you for purchasing this item!" << endl;
				}
			}
			else  if (itemChoice == 3) {
				if (player->getGold() < 300) {
					cout << "I'm sorry but you dont have enough gold to purchase this" << endl;
				}
				else {
					Armor* pArmor = new Armor("Plate Armor", 8, 300);
					player->setArmor(pArmor);
					player->goldLost(pArmor->getCost());
					cout << "Thank you for purchasing this item!" << endl;
				}
			}
		}
		else if (shopChoice == 3) {
			cout << "Thank you for coming!! Come again next time!!" << endl; 
				break;
		}
		system("pause");
		system("cls");
	}
}

void levelUp(EntityClass* player) {
	int statChange = rand() % 4;
	cout << "You Leveld UP!" << endl;
	if (statChange == 0) {
		player->buffMaxHP(10);
	}
	else if (statChange == 1) {
		player->buffPow(2);
	}
	else if (statChange == 2) {
		player->buffVit(2);
	}
	else if (statChange == 3) {
		player->buffDex(2);
	}
	else if (statChange == 4) {
		player->buffAgi(2);
	}
}

void main() {
	int xRoomNum= 0 ;
	int yRoomNum= 0 ;
	int playerChoice;
	int movement;
	string playerName;
	srand(time(NULL));

	cout << "What is your name traveller?" << endl;
	cin >> playerName;

	EntityClass* Player = generatePlayer(playerName);

	while (Player->getCurrentHP() != 0) {
		cout << "What is your action traveler?" << endl;
		cout << "1. Move	2. Rest	3. View Stats	4. Quit" << endl;
		cin >> playerChoice;
		if (playerChoice == 1) {
			cout << "Which direction will you go?" << endl;
			cout << "1. North	2. South	3. East		4. West" << endl;
			cin >> movement;
			if (movement == 1) {
				yRoomNum++;
			}
			else if (movement == 2) {
				yRoomNum--;
			}
			else if (movement == 3) {
				xRoomNum++;
			}
			else if (movement == 4) {
				xRoomNum--;
			}
			if (xRoomNum == 1 && yRoomNum == 1) {
				shop(Player);
			}
			else {
				int encounter = rand() % 99 + 1;
				if (encounter >= 81) {
					cout << "It seems the room is empty traveler" << endl;
				}
				else {
					EntityClass* Enemy = generateEnemy(encounter);
					battle(Player, Enemy);

					if (Player->getCurrentHP() == 0) {
						cout << "You have fallen..." << endl;
						break;
					}
					cout << "You Have Defeted the Enemy!" << endl;
					if (Enemy->getName() == "Goblin") {
						cout << "You gained 100xp and 10 gold " << endl;
						Player->addExp(100);
						Player->addGold(10);
					}
					else if (Enemy->getName() == "Ogre") {
						cout << "You gained 250xp and 50 gold " << endl;
						Player->addExp(250);
						Player->addGold(50);
					}
					else if (Enemy->getName() == "Orc") {
						cout << "You gained 500xp and 100 gold " << endl;
						Player->addExp(500);
						Player->addGold(100);
					}
					else if (Enemy->getName() == "Orc Lord") {
						cout << "You gained 1000xp and 1000 gold " << endl;
						Player->addExp(1000);
						Player->addGold(1000);
					}
					delete Enemy;
					if (Player->getExp() == Player->getReqExp()) {
						levelUp(Player);
					}
				}
			}
		}
		else if (playerChoice == 2) {
			cout << "You have taken a long rest and have healed back to full HP:" << endl;
			Player->Rest();
		}
		else if (playerChoice == 3) {
			showStat(Player);
		}
		else if (playerChoice == 4) {
			break;
		}
		else {
			cout << "That is not a valid choice please choose again" << endl;
		}
		system("pause");
		system("cls");
	}
	delete Player;
}