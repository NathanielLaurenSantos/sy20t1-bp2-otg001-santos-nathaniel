#pragma once
#include <string>
#include <iostream>

using namespace std;

class Armor
{
public:
	Armor(string name, int def, int cost);

	string getName();
	int getDef();
	int getCost();

private:
	string mName;
	int mDef;
	int mCost;
};

