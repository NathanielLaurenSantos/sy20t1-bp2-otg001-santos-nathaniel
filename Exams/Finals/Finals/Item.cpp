#include "Item.h"
#include <iostream>
#include <string>

using namespace std;

Item::Item(string name, int dmg, int cost)
{
	mName = name;
	mBaseDMG = dmg;
	mCost = cost;
}

string Item::getName()
{
	return mName;
}

int Item::getDMG()
{
	return mBaseDMG;
}

int Item::getCost()
{
	return mCost;
}
