#include "Armor.h"
#include <iostream>
#include <string>

using namespace std;

Armor::Armor(string name, int def, int cost)
{
	mName = name;
	mDef = def;
	mCost = cost;
}

string Armor::getName()
{
	return mName;
}

int Armor::getDef()
{
	return mDef;
}

int Armor::getCost()
{
	return mCost;
}
