#pragma once
#include <iostream>
#include <string>

using namespace std;

class Item
{
public:
	Item(string name, int dmg , int cost);

	string getName();
	int getDMG();
	int getCost();
		

private:
	string mName;
	int mBaseDMG;
	int mCost;

};

