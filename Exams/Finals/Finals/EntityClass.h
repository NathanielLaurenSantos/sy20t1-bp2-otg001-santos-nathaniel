#pragma once
#include <string>
#include <iostream>
class Item;
class Armor;

using namespace std;

class EntityClass
{
public:
	
	EntityClass(string name, string pclass, int hp, int pow, int vit, int dex, int agi,Item* weapon, Armor* armor);

	string getName();
	string getClass();
	int getCurrentHP();
	int getMaxHP();
	int getPow();
	int getVit();
	int getDex();
	int getAgi();
	int getExp();
	int getLevel();
	int getReqExp();
	int getGold();
	Item* getWeapon();
	Armor* getArmor();

	void Rest();
	void setHP(float value);
	void buffMaxHP(int buff);
	void buffPow(int buff);
	void buffVit(int buff);
	void buffDex(int buff);
	void buffAgi(int buff);
	void addExp(int xpGain);
	void addGold(int goldGain);
	void goldLost(int lost);
	void setWeapon(Item* newWeapon);
	void setArmor(Armor* newArmor);

	void attack(EntityClass* target);

private:
	string mName;
	string mClass;
	int mMaxHP;
	int mCurrentHP;
	int mPow;
	int mVit;
	int mDex;
	int mAgi;
	int mExp;
	int mGold;
	int mLevel = 1;
	int mRequiredExp =mLevel * 1000;
	Item* mWeapon;
	Armor* mArmor;
};

