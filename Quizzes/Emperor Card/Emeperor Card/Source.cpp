#include<iostream>
#include<time.h>
#include<string>
#include<vector>


using namespace std;

void filldeck(vector<string>& deck, bool round) {
	if (round == 0) {
		deck.push_back("Emperor");
	}
	else if (round == 1) {
		deck.push_back("Slave");
	}
	for (int i = 0; i < 4; i++) {
		deck.push_back("Civilian");
	}

}
int playerMove(vector<string>& deck, int& choice) {
	cout << "Here are your cards: " << endl;
	for (int i = 0; i < deck.size(); i++) {
		cout << i << ". " << deck[i] << endl;
	}
	cout << "What is your Play?" << endl;
	cin >> choice;
	cout << "You chose " << deck[choice] << endl;
	return choice;
}
int aiMove(vector<string>& deck, int& choice) {
	choice = rand() % 5;
	cout << "The Ai chose: " << deck[choice] << endl;
	return choice;
}
string matchUp(vector<string>& aiCard, vector<string>& playerCard, int aiChoice, int playerChoice, string& result) {
	if (playerCard[playerChoice]== "Civilian" && aiCard[aiChoice] == "Civilian") {
		cout << "It's a Draw!" << endl;
		return result = "Draw";
	}
	else if (playerCard[playerChoice] == "Emperor" && aiCard[aiChoice] == "Civilian") {
		cout << "You Win!" << endl;
		return result = "Win";
	}
	else if (playerCard[playerChoice] == "Slave" && aiCard[aiChoice] == "Emperor") {
		cout << "You Win!" << endl;
		return result = "Win";
	}
	else if (playerCard[playerChoice] == "Emperor" && aiCard[aiChoice] == "Slave") {
		cout << "You Lose!" << endl;
		return result = "Lose";
	}
	else if (playerCard[playerChoice] == "Slave" && aiCard[aiChoice] == "Civilian") {
		cout << "You Lose!" << endl;
		return result = "Lose";
	}
}
int calculations(int& money, int bet) {
		money = bet *= 100000;
		cout << "You won " << money << "!" << endl;
		return money;
}
void playRound(int round, int& mm, int& money) {
	bool turnOrder = true;
	int playerChoice = 0;
	int aiChoice = 0;
	int bet;
	string result;
	vector<string> playerDeck;
	vector<string> aiDeck;
			if (round % 2 == 1) {
				turnOrder = !turnOrder;
			}
			filldeck(playerDeck, turnOrder);
			filldeck(aiDeck, !turnOrder);
			cout << "Tonegawa: How much will you wager?" << endl;
			cin >> bet;
			for (int i = 0; i < 5; i++) {
				playerMove(playerDeck, playerChoice);
				aiMove(aiDeck, aiChoice);
				matchUp(aiDeck, playerDeck, aiChoice, playerChoice, result);

				if (playerDeck[playerChoice] == "Emperor" || playerDeck[playerChoice]== "Slave") {
					break;
				}
				else if (aiDeck[aiChoice] == "Emperor" || aiDeck[aiChoice] == "Slave") {
					break;
				}
				playerDeck.erase(playerDeck.begin() + playerChoice);
				aiDeck.erase(aiDeck.begin() + aiChoice);
				system("pause");
				system("cls");
			}
			if (result == "Win") {
				calculations(money, bet);
			}
			else if (result == "Lose") {
				mm -= bet;
			}
			system("pause");
			system("cls");
}
int main() {

	srand(time(0));
	int round = 1;
	int mmLeft = 30;
	int moneyEarned = 0;
	
	while (round <= 12) {
		cout << "Current Round: " << round << endl;
		cout << "MM Left: " << mmLeft << endl;
		cout << "Current Money Earned: " << moneyEarned << endl;
		system("pause");
		system("cls");
		playRound(round, mmLeft, moneyEarned);

		round++;

		if (mmLeft <= 0) {
			break;
		}
		if (mmLeft <= 0){
			cout << "The Eardrum Exploded!!!" << endl;
			cout << "Bad End..." << endl;
		}
		else if (mmLeft > 0, moneyEarned >= 20000000 ){
			cout << "You got the Money and have milimeters to spare" << endl;
			cout << "Best Ending yet!" << endl;
		}
		else if (mmLeft >0, moneyEarned < 20000000) {
			cout << "Welp you survived at least!" << endl;
			cout << "Its a Meh outcome" << endl;
		}
	}
}