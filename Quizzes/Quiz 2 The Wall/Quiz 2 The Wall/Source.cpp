#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "Node.h"
using namespace std;

void createList(Node *n1,Node *n2,Node *n3,Node *n4,Node *n5) {
    cout << "What is your Name Soldier?" << endl;
    cout << "Name of the first soldier is ";
    cin >> n1->name;
    cout << "What is your Name Soldier?" << endl;
    cout << "Name of the second soldier is ";
    cin >> n2->name;
    cout << "What is your Name Soldier?" << endl;
    cout << "Name of the third soldier is ";
    cin >> n3->name;
    cout << "What is your Name Soldier?" << endl;
    cout << "Name of the fourth soldier is ";
    cin >> n4->name;
    cout << "What is your Name Soldier?" << endl;
    cout << "Name of the fifth soldier is ";
    cin >> n5->name;

    n1->next = n2;
    n2->next = n3;
    n3->next = n4;
    n4->next = n5;
    n5->next = n1;
}

void printList(Node* current) {
    Node *tempHead = current ;
    Node *tempCurrent = tempHead;
    do {
        cout << tempCurrent->name << endl;
        tempCurrent = tempCurrent->next;
    } while (tempCurrent != tempHead);
}

void game(Node* tempHead, Node* previous, int soldiers) {
    Node* current = tempHead;

    while (soldiers > 1) {
        system("cls");
        // for randomized starting point
        int randomHead = rand() % soldiers + 1;
        for (int i = 0; i < randomHead; i++) {
            current = current->next;
        }

        cout << "The Cloak was handed to " << current->name << endl << endl;

        //to Print the List
        printList(current);

        //to Pass the cloak
        int randomPass = rand() % soldiers + 1;
        cout << endl << "The number of passes will be " << randomPass << endl << endl;

        for (int i = 0; i < randomPass; i++) {
            previous = current;
            current = current->next;
        }

        //to delete the node
        cout << "The Cloak ended at " << current->name << endl;
        cout << "and so " << current->name << " has been eliminated" << endl;
        previous->next = current->next;
        delete current;
        system("pause");
        system("cls");

        //to print the remainig soldiers
        cout << "The Remaining Soldiers are " << endl;
        printList(previous);
        system("pause");
        system("cls");

        //reintializing current
        current = previous;

        --soldiers;
    }

    cout << "The Soldier that will be going out to get help is " << current->name << endl;

}

void main() {
    srand(time(NULL));
    int numOfSoldiers = 5;
    Node *n1 = new Node;
    Node* n2 = new Node;
    Node* n3 = new Node;
    Node* n4 = new Node;
    Node* n5 = new Node;
    Node* tempHead = n1;
    Node* previous= nullptr;

    //to create and link the list
    createList(n1, n2, n3, n4, n5);

    //to play the game
    game(tempHead, previous, numOfSoldiers);
  

 }