#include "EntityClass.h"
#include <string>
#include <iostream>
#include <time.h>


EntityClass::EntityClass(string name, string job, int hp, int pw, int vit, int agi, int dex)
{
	mName = name;
	mClass = job;
	mHP = hp;
	mPW = pw;
	mVIT = vit;
	mAGI = agi;
	mDEX = dex;
}

void EntityClass::attack(EntityClass* target)
{

	srand(time(NULL));
	float damage = 0;

	//Chance of Hitting
	int hitChance = ((float)mDEX / (float)target->getAGI()) * 100;
	int dodgeChance = rand() % 59 + 20;
	cout << mName << " attacks " << target->getName() << endl;
	if (hitChance > dodgeChance) {
		//Damage
		if (mClass == "Warrior" && target->getClass() == "Assassin") {
			damage = (mPW - target->getVIT()) * .5;
		}
		else if (mClass == "Assassin" && target->getClass() == "Mage") {
			damage = (mPW - target->getVIT()) * .5;
		}
		else if (mClass == "Mage" && target->getClass() == "Warrior") {
			damage = (mPW - target->getVIT()) * .5;
		}
		int totalDamage = (mPW - target->getVIT()) + damage;
		if (totalDamage <= 0) {
			totalDamage = 1;
		}
		cout << "The attack hit! dealing a total damage of " << totalDamage << endl;
		target->setHP(target->getHP() - totalDamage);
	}
	else {
		cout << " The attack missed!" << endl;
	}
}

string EntityClass::getName()
{
	return mName;
}

string EntityClass::getClass()
{
	return mClass;
}

int EntityClass::getHP()
{
	return mHP;
}

int EntityClass::getPW()
{
	return mPW;
}

int EntityClass::getVIT()
{
	return mVIT;
}

int EntityClass::getAGI()
{
	return mAGI;
}

int EntityClass::getDEX()
{
	return mDEX;
}

void EntityClass::setHP(int value)
{
	mHP = value;
	if (mHP < 0) mHP = 0;
}

void EntityClass::setPW(int bonus)
{
	mPW = mPW + bonus;
}

void EntityClass::setVIT(int bonus)
{
	mVIT = mVIT + bonus;
}

void EntityClass::setAGI(int bonus)
{
	mAGI = mAGI + bonus;
}

void EntityClass::setDEX(int bonus)
{
	mDEX = mDEX + bonus;
}

void EntityClass::regen()
{
	float regen = .3;
	int totalRegen = mHP * regen;
	mHP = mHP + totalRegen;
}



