#pragma once
#include <iostream>
#include <string>

using namespace std;


class EntityClass
{
public:

	EntityClass(string name, string job, int hp, int pw, int vit, int agi, int dex);

	void attack(EntityClass* target);

	string getName();
	string getClass();
	//Getter Accesors
	int getHP();
	int getPW();
	int getVIT();
	int getAGI();
	int getDEX();

	//Setter Accessors
	void setHP(int value);
	void setPW(int bonus);
	void setVIT(int bonus);
	void setAGI(int bonus);
	void setDEX(int bonus);

	void regen();
private:
	string mName;
	string mClass;
	int mHP;
	int mPW;
	int mVIT;
	int mAGI;
	int mDEX;
};

