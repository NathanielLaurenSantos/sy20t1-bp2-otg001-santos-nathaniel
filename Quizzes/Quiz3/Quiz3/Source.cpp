#include <iostream>
#include <string>
#include <time.h>
#include "EntityClass.h"

using namespace std;

EntityClass* createPlayer() {
	string name;
	string playerClass;
	cout << "What is your name challenger?" << endl;
	cin >> name;
	while (playerClass != "Warrior" || "Assassin" || "Mage") {
		cout << "What is your class?(Choose between Warrior, Assassin, Mage)" << endl;
		cin >> playerClass;
		if (playerClass == "Warrior") {
			EntityClass* Warrior = new EntityClass(name, "Warrior", 20, 5, 5, 2, 2);
			return Warrior;
		}
		else if (playerClass == "Mage") {
			EntityClass* Mage = new EntityClass(name, "Mage", 20, 7, 1, 2, 2);
			return Mage;
		}
		else if (playerClass == "Assassin") {
			EntityClass* Rouge = new EntityClass(name, "Assassin", 20, 6, 3, 5, 5);
			return Rouge;
		}
		else {
			cout << "That is not a valid class. I'll ask you again. " << endl;
		}
		system("pause");
		system("cls");
	}

}

EntityClass* createEnemy(int roundModifier) {
	int enemyClass = rand() % 2 + 1;
	roundModifier = roundModifier * 2;
	if (enemyClass == 1) {
		EntityClass* Warrior = new EntityClass("Enemy Warrior", "Warrior", 20, 1 + roundModifier, 2 + roundModifier, 1 + roundModifier, 1 + roundModifier);
		return Warrior;
	}
	else if (enemyClass == 2) {
		EntityClass* Mage = new EntityClass("Enemy Mage", "Mage", 20, 3 + roundModifier, 0 + roundModifier, 1 + roundModifier, 1 + roundModifier);
		return Mage;
	}
	else if (enemyClass == 3) {
		EntityClass* Rouge = new EntityClass("Enemy Assassin", "Assassin", 20, 2 + roundModifier, 1 + roundModifier, 2 + roundModifier, 2 + roundModifier);
		return Rouge;
	}
}

void printBattle(EntityClass* player, EntityClass* enemy, int round) {
	system("cls");
	cout << "Round: " << round << endl << endl;
	cout << "Name: " << player->getName() << endl;
	cout << "Class: " << player->getClass() << endl;
	cout << "HP: " << player->getHP() << endl;
	cout << "PW: " << player->getPW() << endl;
	cout << "VIT: " << player->getVIT() << endl;
	cout << "AGI: " << player->getAGI() << endl;
	cout << "DEX: " << player->getDEX() << endl;
	cout << endl;
	cout << "VS" << endl;
	cout << endl;
	cout << "Name: " << enemy->getName() << endl;
	cout << "Class: " << enemy->getClass() << endl;
	cout << "HP: " << enemy->getHP() << endl;
	cout << "PW: " << enemy->getPW() << endl;
	cout << "VIT: " << enemy->getVIT() << endl;
	cout << "AGI: " << enemy->getAGI() << endl;
	cout << "DEX: " << enemy->getDEX() << endl;
}

void main() {
	srand(time(NULL));
	int round = 1;
	EntityClass* player = createPlayer();
	while (player->getHP() != 0) {
		EntityClass* opponent = createEnemy(round);
		printBattle(player, opponent, round);
		do {
			if (player->getAGI() > opponent->getAGI()) {
				if (player->getHP() == 0)break;
				printBattle(player, opponent, round);
				system("pause");
				system("cls");
				player->attack(opponent);
				system("pause");
				system("cls");
				if (opponent->getHP() == 0)break;
				printBattle(player, opponent, round);
				system("pause");
				system("cls");
				opponent->attack(player);
				system("pause");
				system("cls");
			}
			else if(opponent->getAGI() > player->getAGI()){
				if (opponent->getHP() == 0)break;
				printBattle(player, opponent, round);
				system("pause");
				system("cls");
				opponent->attack(player);
				system("pause");
				system("cls");
				if (player->getHP() == 0)break;
				printBattle(player, opponent, round);
				system("pause");
				system("cls");
				player->attack(opponent);
				system("pause");
				system("cls");
				
			}
			if (player->getHP() == 0) break;
		} while (opponent->getHP() != 0);
		delete opponent;
		if (player->getHP() == 0) break;
		if (player->getClass() == "Warrior") {
			cout << "You won!" << endl;
			cout << "You gain the following bonus stats: " << endl;
			cout << "PW +3, and VIT +3" << endl;
			system("pause");
			system("cls");
			player->setPW(3);
			player->setVIT(3);
		}
		else if (player->getClass() == "Assassin") {
			cout << "You won!" << endl;
			cout << "You gain the following bonus stats: " << endl;
			cout << "AGI +3, and DEX +3" << endl;
			system("pause");
			system("cls");
			player->setAGI(3);
			player->setDEX(3);
		}
		else if (player->getClass() == "Mage") {
			cout << "You won!" << endl;
			cout << "You gain the following bonus stats: " << endl;
			cout << "PW +5" << endl;
			system("pause");
			system("cls");
			player->setPW(5);
		}
		player->regen();
		round++;
	}
	cout << "You have died bravely inside the arena " << player->getName() << "!" << endl;
	cout << " You have fought for " << round << " rounds" << endl;
}